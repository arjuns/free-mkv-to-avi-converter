using System;
using System.Windows;
using System.Windows.Media;
using VideoConverter.Configurations;
using VideoConverter.Features;

namespace VideoConverter.Skin
{
    public class ApplicationSkinManager
    {
        public static string RegKey = String.Format("Software\\{0}\\{1}",FeatureSelector.CompanyName,FeatureSelector.ApplicationFeatureSelector.ApplicationName);
        public static ApplicationSkin CurrentSkin { get; private set; }
        public static void ChangeSkin(Window window, ApplicationSkin skin)
        {
            string name = skin.ToString();
            //var acent = ThemeManager.DefaultAccents.First(x => x.Name == name);
            //ThemeManager.ChangeTheme(window, acent, Theme.Light);
            ChangeBorderSkin(skin);
            CurrentSkin = skin;
            ConfigReader.SetSkin((int)CurrentSkin);
            
        }

        public static void ChangeBorderSkin(ApplicationSkin skin)
        {
            string name = skin.ToString() + "Brush";
            var template = (SolidColorBrush)Application.Current.FindResource(name);
            Application.Current.Resources["SkinBrush"] = template;


        }
    }
}