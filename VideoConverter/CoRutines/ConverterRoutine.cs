using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using Caliburn.Micro;
using ConverterCore.FFMpeg;
using VideoConverter.Views.ConvertDialog;
using VideoConverter.Views.FileInfo;
using Action = System.Action;

namespace VideoConverter.CoRutines
{
    public class ConverterRoutine : IResult
    {
        private readonly string outputFolder;
        private readonly VideoFormat outputFormat;

        private readonly List<InputVideoViewModel> source;



        public ConverterRoutine(string outputFolder, VideoFormat outputFormat)
        {
            this.outputFolder = outputFolder;
            this.outputFormat = outputFormat;
            source = new List<InputVideoViewModel>();
        }

        public void AddSource(InputVideoViewModel model)
        {
            this.source.Add(model);
        }


        public void Execute(ActionExecutionContext context)
        {

            Dispatcher currentDispatcher = Dispatcher.CurrentDispatcher;


            
            Task.Factory.StartNew(() =>
                {
                    Convert(currentDispatcher);
                    Completed(this, new ResultCompletionEventArgs());
                });

        }

        private void Convert(Dispatcher dispatcher)
        {
            AutoResetEvent evt = new AutoResetEvent(false); //Convert one at a time
            EncodeFinishedEventArgs result = null;


            foreach (var media in source)
            {


                SingleVideoEncodingService service = new SingleVideoEncodingService();
                service.OutputFolder = this.outputFolder;
                service.OutputFormat = this.outputFormat;
                InputVideoViewModel closureMedia = media;
                service.Done += (a, b) =>
                {
                    result = b;
                    if (b.IsCancelled)
                        closureMedia.Status = VideoConversionState.Cancelled;

                    closureMedia.Status = result.Success ?
                       VideoConversionState.Successful :
                       VideoConversionState.Errored;
                    evt.Set();


                };
                media.Status = VideoConversionState.Pending;
                service.BeginEncodeVideo(media.MediaInfo);
                evt.WaitOne();

            }

        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

    }

    public static class TaskExtension
    {
        public static Task Run(this Action action)
        {
            return  Task.Factory.StartNew(action);
        }
    }
}