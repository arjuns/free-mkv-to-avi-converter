using System;
using Caliburn.Micro;
using VideoConverter.Views.ConvertDialog;

namespace VideoConverter.CoRutines
{
    public class ShowConvertDialogRoutine : IResult
    {
        private readonly OptionDialogViewModel dialog;
        public ConvertCommand ConversionParam { get; set; }
        public ShowConvertDialogRoutine()
        {
            dialog = new OptionDialogViewModel();
        }
        public void Execute(ActionExecutionContext context)
        {
            ConversionParam = null;
            var manager = IoC.Get<IWindowManager>();
            if (manager.ShowDialog(dialog) == true)
            {
                ConversionParam = new ConvertCommand
                    {
                        OutputFolder = dialog.OutputFolder,
                        OutputFormat = dialog.SelectedOutputFormat
                    };

            }

            Completed(this, new ResultCompletionEventArgs());
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

     
    }
}