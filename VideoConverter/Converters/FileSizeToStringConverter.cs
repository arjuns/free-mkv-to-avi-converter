using System;
using System.Globalization;
using System.IO;
using System.Windows.Data;

namespace VideoConverter.Converters
{
    public class FileSizeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FileInfo info = new FileInfo(value.ToString());
            return Utility.GetSizeLiteral(info.Length);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}