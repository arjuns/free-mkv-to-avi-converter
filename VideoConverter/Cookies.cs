﻿using System;

namespace VideoConverter
{
    class Cookies
    {
    }
    public static class Utility
    {
        public static string GetSizeLiteral(double fileSize)
        {
            string[] sizes = { "B", "KB", "MB", "GB" };
            int order = 0;
            while (fileSize >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                fileSize = fileSize / 1024;
            }

            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            return String.Format("{0:0.#} {1}", fileSize, sizes[order]);
        }
        public static string FormatToStringLiteral(this TimeSpan span)
        {
            DateTime time=new DateTime(span.Ticks);
            if(time.Hour>0)
            return time.ToString("HH:mm:ss");
            return time.ToString("mm:ss");
        }
    }
}
