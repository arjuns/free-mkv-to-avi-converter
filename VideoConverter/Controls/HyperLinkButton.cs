﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace VideoConverter.Controls
{
    public class HyperLinkButton : Button
    {
        public static readonly DependencyProperty IconSourceProperty = DependencyProperty.Register(
            "IconSource", typeof(ImageSource), typeof(HyperLinkButton), new PropertyMetadata(default(ImageSource)));

        static HyperLinkButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(HyperLinkButton), new FrameworkPropertyMetadata(typeof(HyperLinkButton)));
        }

        public ImageSource IconSource
        {
            get
            {
                return (ImageSource)this.GetValue(IconSourceProperty);
            }
            set
            {
                this.SetValue(IconSourceProperty, value);
            }
        }

    }


    

    public class HeaderControl : ContentControl
    {
        static HeaderControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(HeaderControl), new FrameworkPropertyMetadata(typeof(HeaderControl)));
        }

        public static readonly DependencyProperty FirstColorPorperty = DependencyProperty.Register(
            "FirstColor", typeof(Brush), typeof(HeaderControl), new PropertyMetadata(default(Brush)));

        public Brush FirstColor
        {
            get
            {
                return (Brush)this.GetValue(FirstColorPorperty);
            }
            set
            {
                this.SetValue(FirstColorPorperty, value);
            }
        }

        public static readonly DependencyProperty SecondColorPorperty = DependencyProperty.Register(
            "SecondColor", typeof(Brush), typeof(HeaderControl), new PropertyMetadata(default(Brush)));

        public Brush SecondColor
        {
            get
            {
                return (Brush)this.GetValue(SecondColorPorperty);
            }
            set
            {
                this.SetValue(SecondColorPorperty, value);
            }
        }
    }
}