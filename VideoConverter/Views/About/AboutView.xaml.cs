﻿using System.Diagnostics;
using System.Windows;
using VideoConverter.Skin;

namespace VideoConverter.Views.About
{
    /// <summary>
    /// Interaction logic for AboutView.xaml
    /// </summary>
    public partial class AboutView 
    {
        public AboutView()
        {
            InitializeComponent();
            this.Loaded += WindowLoaded;
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            ApplicationSkinManager.ChangeSkin(this, ApplicationSkinManager.CurrentSkin);
        }

        private void OpenEmailClient(object sender, RoutedEventArgs e)
        {
            Process.Start("mailto:support@freedomsoftwarecompany.com");
        }
        private void OpenWebSite(object sender, RoutedEventArgs e)
        {
            Process.Start("http://www.freedomsoftwarecompany.com");
        }
    }
}
