﻿using Caliburn.Micro;
using VideoConverter.Features;

namespace VideoConverter.Views.About
{
    public class AboutViewModel : Screen
    {
        public string ApplicationName
        {
            get { return FeatureSelector.ApplicationFeatureSelector.ApplicationName; }
            set
            {

            }
        }

        public override string DisplayName
        {
            get { return "About"; }
            set{}
        }
    }
}
