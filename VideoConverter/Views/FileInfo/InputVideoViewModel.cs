﻿using System;
using System.IO;
using Caliburn.Micro;
using ConverterCore.FFMpeg;

namespace VideoConverter.Views.FileInfo
{

    public enum VideoConversionState
    {
        NotStarted,
        Pending,
        Errored,
        Successful,
        Cancelled
    }
    public class InputVideoViewModel : PropertyChangedBase
    {
        private string fullPath;
        private VideoConversionState status;
        private double progress;
        private string thumbImage;
        private TimeSpan videoLength;

        public string FullPath
        {
            get { return fullPath; }
            set
            {
                if (value == fullPath) return;
                fullPath = value;
                NotifyOfPropertyChange(() => FullPath);
                NotifyOfPropertyChange(() => FileName);
                NotifyOfPropertyChange(() => FileFormat);
            }
        }
        public string FileFormat
        {
            get
            {
                return Path.GetExtension(fullPath).ToUpper();
            }
        }

        public string ThumbImage
        {
            get { return thumbImage; }
            set
            {
                if (value == thumbImage) return;
                thumbImage = value;
                NotifyOfPropertyChange(() => ThumbImage);
            }
        }

        public VideoConversionState Status
        {
            get { return status; }
            set
            {
                if (Equals(value, status)) return;
                status = value;
                NotifyOfPropertyChange(() => Status);
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public bool IsBusy
        {
            get { return Status == VideoConversionState.Pending; }
        }

        public string FileName
        {
            get { return Path.GetFileNameWithoutExtension(fullPath); }
        }

        public void CancelOperation()
        {
            if (this.IsBusy)
            {
                IoC.Get<IEventAggregator>().Publish(this);
            }
        }

        public TimeSpan VideoLength
        {
            get { return videoLength; }
            set
            {
                if (value.Equals(videoLength)) return;
                videoLength = value;
                NotifyOfPropertyChange(() => VideoLength);
            }
        }


        public double Progress
        {
            get { return progress; }
            set
            {
                if (value.Equals(progress)) return;
                progress = value;
                NotifyOfPropertyChange(() => Progress);
            }
        }

        public MediaInformation MediaInfo { get; set; }


    }

}
