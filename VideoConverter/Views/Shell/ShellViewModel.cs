﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using Caliburn.Micro;
using ConverterCore.Processors;
using VideoConverter.CoRutines;
using VideoConverter.Controls.DragDrop;
using VideoConverter.Features;
using VideoConverter.Skin;
using VideoConverter.Store;
using VideoConverter.Views.About;
using VideoConverter.Views.FileInfo;
using DataFormats = System.Windows.DataFormats;
using DataObject = System.Windows.DataObject;
using DragDropEffects = System.Windows.DragDropEffects;
using IDropTarget = VideoConverter.Controls.DragDrop.IDropTarget;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using Screen = Caliburn.Micro.Screen;

namespace VideoConverter.Views.Shell
{
    public class ShellViewModel : Screen, IShellViewModel, IDropTarget
    {
        private readonly IThumbnailStore thumbStore;
        private InputVideoViewModel selectedFile;
        private ApplicationSkin selectedSkin;
        public ObservableCollection<InputVideoViewModel> Files { get; set; }

        public ShellViewModel(IThumbnailStore thumbStore)
        {
            thumbStore.Clean();
            this.thumbStore = thumbStore;
            Files = new ObservableCollection<InputVideoViewModel>();
            Skins =
                new ObservableCollection<ApplicationSkin>(new[]
                    {
                        ApplicationSkin.Blue,
                        ApplicationSkin.Green,
                        ApplicationSkin.Orange,
                        ApplicationSkin.Purple,
                        ApplicationSkin.Red
                    });
        }


        public InputVideoViewModel SelectedFile
        {
            get { return selectedFile; }
            set
            {
                if (Equals(value, selectedFile)) return;
                selectedFile = value;
                NotifyOfPropertyChange(() => SelectedFile);
            }
        }

        public ApplicationSkin SelectedSkin
        {
            get { return selectedSkin; }
            set
            {
                if (value == selectedSkin) return;
                selectedSkin = value;
                NotifyOfPropertyChange(() => SelectedSkin);
            }
        }

        public ObservableCollection<ApplicationSkin> Skins { get; private set; }


        public override string DisplayName
        {
            get { return FeatureSelector.ApplicationFeatureSelector.ApplicationName; }
            set
            {

            }
        }



        public void AddFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "mkv files(*.mkv)|*.mkv|avi files(*.avi)|*.avi";
            bool? showDialog = ofd.ShowDialog();
            if (showDialog == true)
            {
                AddFileInternal(ofd.FileName);
            }
        }

        private void AddFileInternal(string filename)
        {

            InputVideoViewModel vdo = new InputVideoViewModel { FullPath = filename };
            Files.Add(vdo);

            Task.Factory.StartNew(() =>
                {
                    MediaInformationExtractor infoExtractor = new MediaInformationExtractor(vdo.FullPath);
                    vdo.MediaInfo = infoExtractor.GetInfo();
                    vdo.VideoLength = vdo.MediaInfo.Duration;
                   
                }).ContinueWith((a) =>
                    {
                        var thumbextractor = new ThumbnailExtractor(filename,
                                                                              this.thumbStore.GetUniqueThubnailImagePath
                                                                                  (), vdo.MediaInfo);
                        bool extracted = thumbextractor.ExtractThumbnal();
                        if (extracted)
                            vdo.ThumbImage = thumbextractor.OutputFileName;
                    });

        }

        public void AddFolder()
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                DragFileSelector selector = new DragFileSelector();
                List<string> validFiles = selector.FilterValidFiles(Directory.GetFiles(dialog.SelectedPath).ToArray());
                foreach (var validFile in validFiles)
                {
                    AddFileInternal(validFile);
                    
                }

            }
        }


        public void RemoveSelected()
        {

            Remove(this.SelectedFile);

        }
        public void RemoveAll()
        {
            this.Files.Clear();
        }


        public void Remove(InputVideoViewModel model)
        {
            if (model.IsBusy)
            {
                MessageBoxResult result = MessageBox.Show("Media conversion is in progress! \n Do you want to cancel and remove", FeatureSelector.ApplicationFeatureSelector.ApplicationName, MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.No)
                    return;
                model.CancelOperation();
            }
            this.Files.Remove(model);
        }

        public IEnumerable<IResult> ConvertAll()
        {
            var dialog = new ShowConvertDialogRoutine();
            yield return dialog;

            if (dialog.ConversionParam != null)
            {
                foreach (var model in Files)
                {

                    var routing = new ConverterRoutine(dialog.ConversionParam.OutputFolder,dialog.ConversionParam.OutputFormat);
                    routing.AddSource(model);
                    yield return routing;
                }
            }

        }

        public void ShowAboutDialog()
        {
            var evt = IoC.Get<IWindowManager>();
            evt.ShowDialog(new AboutViewModel());

        }

        void IDropTarget.DragOver(IDropInfo dropInfo)
        {
            var data = dropInfo.Data as DataObject;
            if (data != null)
            {
                if (data.GetDataPresent(DataFormats.FileDrop))
                {
                    dropInfo.DropTargetAdorner = DropTargetAdorners.Highlight;
                    dropInfo.Effects = DragDropEffects.Move;



                }

            }
        }

        void IDropTarget.Drop(IDropInfo dropInfo)
        {
            var data = dropInfo.Data as DataObject;
            if (data != null)
            {
                if (data.GetDataPresent(DataFormats.FileDrop))
                {
                    string[] files = (string[])data.GetData(DataFormats.FileDrop);
                    DragFileSelector selector = new DragFileSelector();
                    var valid = selector.FilterValidFiles(files).ToList();
                    valid.ForEach(AddFileInternal);

                }

            }
        }
    }


}

