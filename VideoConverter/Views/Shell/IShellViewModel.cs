﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Caliburn.Micro;
using VideoConverter.Views.FileInfo;

namespace VideoConverter.Views.Shell
{
    public interface IShellViewModel
    {
        ObservableCollection<InputVideoViewModel> Files { get; set; }
        void AddFile();
        void AddFolder();
        void RemoveSelected();
        void RemoveAll();
        IEnumerable<IResult> ConvertAll();
    }
}