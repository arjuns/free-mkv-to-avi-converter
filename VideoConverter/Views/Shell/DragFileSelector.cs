﻿using System.Collections.Generic;
using VideoConverter.Features;

namespace VideoConverter.Views.Shell
{
    public class DragFileSelector
    {
       

        public List<string> FilterValidFiles(string[] files)
        {
            List<string> valid = new List<string>();
            foreach (var file in files)
            {
                foreach (var validExtension in FeatureSelector.ApplicationFeatureSelector.GetAvailableFileFormat())
                {
                    if (file.EndsWith(validExtension))
                        valid.Add(file);
                }
            }
            return valid;
        }
    }
    
}