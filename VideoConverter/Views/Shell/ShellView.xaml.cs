﻿using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using VideoConverter.Configurations;
using VideoConverter.Features;
using VideoConverter.Skin;

namespace VideoConverter.Views.Shell
{
    /// <summary>
    /// Interaction logic for Shell.xaml
    /// </summary>
    public partial class ShellView 
    {
        public ShellView()
        {


            InitializeComponent();
            this.Loaded += WindowLoaded;
            this.Closing += CloseAllFFMpeg;
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            var skin = (ApplicationSkin)ConfigReader.GetSkin();
            ApplicationSkinManager.ChangeSkin(this, skin);

        }

        private void CloseAllFFMpeg(object sender, CancelEventArgs e)
        {
            Process[] processesByName = Process.GetProcessesByName("ffmpeg");
            if (processesByName.Length > 0)
            {
                MessageBoxResult boxResult = MessageBox.Show("It looks that media conversion(s) are running.\r Are you sure you want to close?", FeatureSelector.ApplicationFeatureSelector.ApplicationName, MessageBoxButton.YesNo);
                if (boxResult == MessageBoxResult.Yes)
                    processesByName.ToList().ForEach(x => x.Kill());
                else
                {
                    e.Cancel = true;
                }
            }

        }

        private void ChangeSkin(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                ApplicationSkinManager.ChangeSkin(this, (ApplicationSkin)button.Tag);
            }
        }
    }
}
