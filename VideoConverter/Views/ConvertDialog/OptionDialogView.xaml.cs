﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using VideoConverter.Configurations;
using VideoConverter.Skin;
using ListBox = System.Windows.Controls.ListBox;

namespace VideoConverter.Views.ConvertDialog
{
    /// <summary>
    /// Interaction logic for ConvertDialogView.xaml
    /// </summary>
    public partial class OptionDialogView 
    {
        public OptionDialogView()
        {
            InitializeComponent();
            this.Loaded += WindowLoaded;
            this.Closing += WindowClosing;



        }



        private void WindowClosing(object sender, CancelEventArgs e)
        {
            ////save configuration folder
            if (!string.IsNullOrEmpty(this.splitTextBox.Text))
                ConfigReader.OutputFolder = this.splitTextBox.Text;

        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            this.splitTextBox.Text = ConfigReader.OutputFolder;
            ApplicationSkinManager.ChangeSkin(this, ApplicationSkinManager.CurrentSkin);
        }


        private void FolderOptionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listBox = sender as ListBox;
            if (listBox != null && listBox.SelectedItem != null)
            {
                if (listBox.SelectedIndex == listBox.Items.Count - 1)
                {
                    using (FolderBrowserDialog fd = new FolderBrowserDialog())
                    {
                        DialogResult result = fd.ShowDialog();
                        if (result == System.Windows.Forms.DialogResult.OK)
                        {
                            this.splitTextBox.Text = fd.SelectedPath;

                        }
                    }

                }
                else
                {
                    this.splitTextBox.Text = ((KeyValue)listBox.SelectedItem).FolderName;
                }
                splitButton.IsOpen = false;

            }

        }

        private void SplitOpened(object sender, RoutedEventArgs e)
        {
            this.splitBox.SelectedIndex = -1;
        }
    }

}
