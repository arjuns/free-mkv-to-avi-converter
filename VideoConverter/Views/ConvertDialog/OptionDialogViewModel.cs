﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Caliburn.Micro;
using VideoConverter.Features;
using Screen = Caliburn.Micro.Screen;

namespace VideoConverter.Views.ConvertDialog
{

    public class KeyValue : PropertyChangedBase
    {
        private string folderName;
        private string displayName;

        public String FolderName
        {
            get { return folderName; }
            set
            {
                if (value == folderName) return;
                folderName = value;
                NotifyOfPropertyChange(() => FolderName);
            }
        }

        public string DisplayName
        {
            get { return displayName; }
            set
            {
                if (value == displayName) return;
                displayName = value;
                NotifyOfPropertyChange(() => DisplayName);
            }
        }

        public static KeyValue Make(string folder)
        {
            return new KeyValue() {DisplayName = folder, FolderName = folder};
        }
    }

    public class Folders
    {
        public static string GetFolderPath(CSIDL folder)
        {
            StringBuilder SB = new StringBuilder(259);
            SHGetFolderPath(IntPtr.Zero, (int) folder, IntPtr.Zero, 0x0000, SB);
            return SB.ToString();
        }

        [DllImport("shell32.dll")]
        private static extern int SHGetFolderPath(IntPtr hwndOwner, int nFolder, IntPtr hToken, uint dwFlags,
                                                  [Out] StringBuilder pszPath);

        public enum CSIDL
        {
            MYVIDEO = 14,
            USER_PROFILE = 0x0028
        }
    }

    public class OptionDialogViewModel : Screen, IOptionDialogViewModel
    {
        private VideoFormat outputFormat;
        private string outputFolder;

        public OptionDialogViewModel()
        {

            OutputFolders = new ObservableCollection<KeyValue>();
          //  OutputFolders.Add(KeyValue.Make(Environment.GetFolderPath(Environment.SpecialFolder.MyVideos)));
            OutputFolders.Add(KeyValue.Make(Folders.GetFolderPath(Folders.CSIDL.MYVIDEO)));
            OutputFolders.Add(KeyValue.Make(Environment.GetFolderPath(Environment.SpecialFolder.MyMusic)));
            OutputFolders.Add(KeyValue.Make(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)));
            OutputFolders.Add(KeyValue.Make(Environment.GetFolderPath(Environment.SpecialFolder.Desktop)));
            OutputFolders.Add(KeyValue.Make(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)));
            OutputFolders.Add(new KeyValue() {DisplayName = "...Browse.."});
        }


        public VideoFormat SelectedOutputFormat
        {
            get { return outputFormat; }
            set
            {
                if (value == outputFormat) return;
                outputFormat = value;
                NotifyOfPropertyChange(() => SelectedOutputFormat);
            }
        }

        public string OutputFolder
        {
            get { return outputFolder; }
            set
            {
                if (value == outputFolder) return;
                outputFolder = value;
                NotifyOfPropertyChange(() => OutputFolder);
            }
        }

        public ObservableCollection<KeyValue> OutputFolders { get; set; }

        public ObservableCollection<VideoFormat> OutputFormats
        {
            get
            {
                return
                    new ObservableCollection<VideoFormat>(
                        FeatureSelector.ApplicationFeatureSelector.GetValidVideoFormat());
            }
        }



        public override string DisplayName
        {
            get { return "Options"; }
            set { }
        }

        public void Start()
        {
            if (!Directory.Exists(this.OutputFolder))
            {

                MessageBox.Show(@"Please make sure output directory exists!",
                                FeatureSelector.ApplicationFeatureSelector.ApplicationName,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            TryClose(true);
        }


    }


}
