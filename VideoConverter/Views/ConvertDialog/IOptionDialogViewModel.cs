﻿namespace VideoConverter.Views.ConvertDialog
{
    public interface IOptionDialogViewModel
    {
        VideoFormat SelectedOutputFormat { get; set; }
        string OutputFolder { get;  }
    }
}