﻿using System;
using System.IO;
using VideoConverter.Views.ConvertDialog;

namespace VideoConverter.Store
{
    public class ThumbnailStore : IThumbnailStore
    {
        public string GetUniqueThubnailImagePath()
        {
            EnsureTempDirectory();
            string tempLocation = Path.Combine(GetThumbDirectory(), String.Format("{0}.png", Guid.NewGuid()));
            string dirName = Folders.GetFolderPath(Folders.CSIDL.USER_PROFILE);
            return Path.Combine(dirName, tempLocation);
        }



        public void EnsureTempDirectory()
        {
            var dir = GetThumbDirectory();
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }

        private static string GetThumbDirectory()
        {
            string dirName = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var dir = Path.Combine(dirName, "FSC\\Thumbs");
            return dir;
        }

        public void Clean()
        {
            try
            {
                Directory.Delete(GetThumbDirectory(), true);
                EnsureTempDirectory();
            }
            catch { }
        }
    }
}