﻿using System;
using System.IO;
using Microsoft.Win32;
using VideoConverter.Features;
using VideoConverter.Views.ConvertDialog;

namespace VideoConverter.Configurations
{
    public static class ConfigReader
    {
        private static readonly string RegKey = String.Format("Software\\{0}\\{1}", FeatureSelector.CompanyName,
            FeatureSelector.ApplicationFeatureSelector.ApplicationName);

        private const string OutputFolderName = "OutputFolder";
        private const string SkinName = "Skin";


        
        public static string OutputFolder
        {
            get { return GetOutputFolder(); }
            set
            {
                SaveOutputFolder(value);
            }
        }
        private static RegistryKey RootKey
        {
            get
            {
                return Registry.CurrentUser.OpenSubKey(RegKey,true);
            }
        }

        static ConfigReader()
        {
            EnsureRootRegKey();
        }
        private static void EnsureRootRegKey()
        {
            RegistryKey fsc = Registry.CurrentUser.OpenSubKey(RegKey, true);
            if (fsc == null)
                Registry.CurrentUser.CreateSubKey(RegKey);
        }
        private static string GetOutputFolder()
        {
            var value = RootKey.GetValue(OutputFolderName);
            if (value == null || !Directory.Exists(value.ToString()))
            {
                var combine = CreateOutputFolderIfInvalid();
                RootKey.SetValue(OutputFolderName, combine);
            }
            return (string)RootKey.GetValue(OutputFolderName);
        }

        private static string CreateOutputFolderIfInvalid()
        {
            string location = Folders.GetFolderPath(Folders.CSIDL.MYVIDEO);
            //string location = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
            
            if (!Directory.Exists(location))
                Directory.CreateDirectory(location);
            return location;
        }

        private static void SaveOutputFolder(string folderName)
        {
            RootKey.SetValue(OutputFolderName, folderName);
        }

        public static int GetSkin()
        {
            using (RootKey)
            {
                var value = RootKey.GetValue(SkinName);
                if (value == null)
                {
                    RootKey.SetValue(SkinName, 1);
                }
                return (int)RootKey.GetValue(SkinName);    
            }
            
        }
        public static void SetSkin(int skin)
        {
            RootKey.SetValue(SkinName,skin);
        }
    }
}
