﻿using System.Collections.Generic;
using VideoConverter.Views.ConvertDialog;

namespace VideoConverter.Features
{
    public class MkvToAVIApplication : FeatureSelector
    {

        public override string ApplicationName
        {
            get { return "Free MKV to AVI Converter"; }
        }
        public override Dictionary<VideoFormat, string> CreateFormatMap()
        {
            var formatMap = new Dictionary<VideoFormat, string>();
            formatMap.Add(VideoFormat.Mkv, ".mkv");
            formatMap.Add(VideoFormat.Avi, ".avi");
            return formatMap;
        }
    }
}