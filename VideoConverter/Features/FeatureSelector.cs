﻿using System.Collections.Generic;
using System.Linq;
using VideoConverter.Views.ConvertDialog;

namespace VideoConverter.Features
{
    public abstract class FeatureSelector
    {
        public static string CompanyName = @"FreedomSoftwareCompany";
        public abstract string ApplicationName { get; }

        public string[] GetAvailableFileFormat()
        {
            return CreateFormatMap().Values.ToArray();
        }
        public VideoFormat[] GetValidVideoFormat()
        {
            return CreateFormatMap().Keys.ToArray();
        }
        public  string GetExtensionFor(VideoFormat format)
        {
            return CreateFormatMap()[format];
        }

        public abstract Dictionary<VideoFormat, string> CreateFormatMap();

        public static FeatureSelector ApplicationFeatureSelector
        {
            get
            {
                return new MkvToMP4Application();
                //return new MkvToAVIApplication();
            }
        }


    }
}