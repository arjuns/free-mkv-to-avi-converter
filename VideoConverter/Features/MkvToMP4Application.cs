﻿using System.Collections.Generic;
using VideoConverter.Views.ConvertDialog;

namespace VideoConverter.Features
{
    public class MkvToMP4Application : FeatureSelector
    {

        public override string ApplicationName
        {
            get { return "Free MKV to MP4 Converter"; }
        }
        public override Dictionary<VideoFormat, string> CreateFormatMap()
        {
            var formatMap = new Dictionary<VideoFormat, string>();
            formatMap.Add(VideoFormat.Mkv, ".mkv");
            formatMap.Add(VideoFormat.Mp4, ".mp4");
            return formatMap;
        }
    }
}
