﻿using Caliburn.Micro;

namespace VideoConverter.Services
{
    public class CaliburnModule : IModule
    {
        public void RegisterServices(IContainer container)
        {
            container.RegisterSingle<IWindowManager, WindowManager>(); //singletone
            container.RegisterSingle<IEventAggregator, EventAggregator>(); //singletone
        }
    }
}
