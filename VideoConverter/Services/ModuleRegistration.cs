﻿using System;
using System.Linq;
using System.Reflection;

namespace VideoConverter.Services
{
    public static class ModuleRegistration
    {
        public static void RegisterPackages(this IContainer container, Assembly asm)
        {
            var types = asm.GetExportedTypes().Where(x => (typeof(IModule).IsAssignableFrom(x)) && !x.IsAbstract && x.IsClass).ToList();

            foreach (var package in types)
            {
                var type = asm.GetType(package.FullName);
                IModule instance = (IModule)Activator.CreateInstance(type);
                instance.RegisterServices(container);
            }
        }
    }
}