﻿using System;

namespace VideoConverter.Services
{
    public interface IContainer
    {
        void RegisterSingle<RegisterType, RegisterImplementation>()
            where RegisterType : class
            where RegisterImplementation : class ,RegisterType;
        void Register<RegisterType, RegisterImplementation>()
            where RegisterType : class
            where RegisterImplementation : class ,RegisterType;
        TSource GetInstance<TSource>() where TSource:class;
        Object GetInstance(Type type);
        Object[] GetAllInstances(Type type);
        void Verify();

    }
}