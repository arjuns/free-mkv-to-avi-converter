﻿using System;
using System.IO;
using Caliburn.Micro;
using ConverterCore.Encoders;
using ConverterCore.FFMpeg;
using VideoConverter.Features;
using VideoConverter.Views.ConvertDialog;
using VideoConverter.Views.FileInfo;

namespace VideoConverter
{
    public interface IVideoEncodingService
    {
        MediaInformation GetVideoInfo(string file);
        void Run();
        event EventHandler<EncodeProgressEventArgs> Progress;
        event EventHandler<EncodeFinishedEventArgs> SingleDone;
        string GenerateThumbImage(string file, string thumbPath, MediaInformation mediaInformation);



    }

    public class SingleVideoEncodingService : IHandle<InputVideoViewModel>
    {
        public string OutputFolder { get; set; }
        public VideoFormat OutputFormat { get; set; }

        private void RemoveIfFileExists(string outputFile)
        {
            if (File.Exists(outputFile))
                File.Delete(outputFile);
        }

        private void EnsureDirectoryExists(string outputFolder)
        {

            if (!Directory.Exists(outputFolder))
                Directory.CreateDirectory(outputFolder);

        }

        private string GetSaveFileName(MediaInformation file, string outputFolder, VideoFormat format)
        {

            var extension = FeatureSelector.ApplicationFeatureSelector.GetExtensionFor(format);
            string fileNameOnly = Path.GetFileNameWithoutExtension(file.OriginalFilePath) + extension;
            return Path.Combine(outputFolder, fileNameOnly);
        }

        public event EventHandler<EncodeProgressEventArgs> Progress;
        public event EventHandler<EncodeFinishedEventArgs> Done;

        private EncoderBase actualEncoder;

        public SingleVideoEncodingService()
        {
            IoC.Get<IEventAggregator>().Subscribe(this);
        }

        public void BeginEncodeVideo(MediaInformation media)
        {
            var outputFile = GetSaveFileName(media, OutputFolder, OutputFormat);
            EnsureDirectoryExists(OutputFolder);
            RemoveIfFileExists(outputFile);
            actualEncoder = EncoderFactory.GetEncoder(media, outputFile, OutputFormat);
            HookUpHandlers();
            actualEncoder.BeginEncode();

        }

        private void HookUpHandlers()
        {
            actualEncoder.Progress += (a, b) =>
                {
                    if (this.Progress != null)
                        Progress(a, b);
                };
            actualEncoder.VideoFinished += (a, b) =>
                {
                    if (this.Done != null)
                        Done(a, b);
                };
        }

        public void Handle(InputVideoViewModel message)
        {
            actualEncoder.Cancel();
        }
    }

    //public class VideoEncodingService : IVideoEncodingService
    //{

    //    private readonly List<EncoderBase> encoderQueue;
    //    public string OutputFolder { get; set; }
    //    public VideoFormat OutputFormat { get; set; }

    //    public VideoEncodingService()
    //    {
    //        encoderQueue = new List<EncoderBase>();
    //    }
    //    public void AddEncoder(MediaInformation media)
    //    {
    //        var outputFile = GetSaveFileName(media, OutputFolder, OutputFormat);
    //        EnsureDirectoryExists(OutputFolder);
    //        RemoveIfFileExists(outputFile);
    //        EncoderBase actualEncoder = EncoderFactory.GetEncoder(media, outputFile, OutputFormat);

    //        actualEncoder.Progress += (a, b) =>
    //        {
    //            if (this.Progress != null)
    //                Progress(a, b);
    //        };
    //        actualEncoder.VideoFinished += (a, b) =>
    //        {

    //            if (this.SingleDone != null)
    //                SingleDone(a, b);
    //        };

    //        this.encoderQueue.Add(actualEncoder);

    //    }


    //    public MediaInformation GetVideoInfo(string file)
    //    {
    //        return new MediaInformationExtractor(file).GetInfo();
    //    }


    //    public void Run()
    //    {
    //        foreach (EncoderBase encoder in encoderQueue)
    //        {
    //            encoder.Encode();
    //        }

    //    }

    //    public event EventHandler<EncodeProgressEventArgs> Progress;
    //    public event EventHandler<EncodeFinishedEventArgs> SingleDone;


    //    public string GenerateThumbImage(string file, string thumbPath, MediaInformation mediaInformation)
    //    {
    //        bool extracted = new ThumbnailExtractor(file, thumbPath, mediaInformation).ExtractThumbnal();
    //        if (extracted)
    //            return thumbPath;
    //        return "";

    //    }



    //    public void Cancel(MediaInformation media)
    //    {
    //        EncoderBase encoder = encoderQueue.FirstOrDefault(x => x.MediaInfo == media);
    //        if (encoder != null)
    //            encoder.Cancel();

    //    }

    //    private void RemoveIfFileExists(string outputFile)
    //    {
    //        if (File.Exists(outputFile))
    //            File.Delete(outputFile);
    //    }

    //    private void EnsureDirectoryExists(string outputFolder)
    //    {

    //        if (!Directory.Exists(outputFolder))
    //            Directory.CreateDirectory(outputFolder);

    //    }

    //    private string GetSaveFileName(MediaInformation file, string outputFolder, VideoFormat format)
    //    {
    //        string extension;
    //        switch (format)
    //        {
    //            case VideoFormat.Mkv:
    //                extension = ".mkv";
    //                break;
    //            case VideoFormat.Avi:
    //                extension = ".avi";
    //                break;
    //            default: throw new InvalidOperationException("Invalid video format");

    //        }
    //        string fileNameOnly = Path.GetFileNameWithoutExtension(file.OriginalFilePath) + extension;
    //        return Path.Combine(outputFolder, fileNameOnly);
    //    }


    //}


    public static class EncoderFactory
    {
        public static EncoderBase GetEncoder(MediaInformation inputVideo, string outputFile, VideoFormat outputFormat)
        {
            switch (outputFormat)
            {
                case VideoFormat.Mkv:
                    return new MkvConverter(inputVideo, outputFile);
                    break;
                case VideoFormat.Avi:
                    return new AVIConverter(inputVideo, outputFile);
                case VideoFormat.Mp4:
                    return new MP4Converter(inputVideo, outputFile);
                default:
                    throw new ArgumentOutOfRangeException("outputFormat");
            }
        }
    }

}