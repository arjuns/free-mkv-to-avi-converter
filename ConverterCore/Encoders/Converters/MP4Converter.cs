using System;
using ConverterCore.FFMpeg;
using ConverterCore.Processors;

namespace ConverterCore.Encoders
{
    public class MP4Converter : EncoderBase
    {
        public MP4Converter(MediaInformation mediaInfo,
                            string outputFile)
            : base(mediaInfo, outputFile)
        {

        }
        protected override FFMpegArgument CreateParameter()
        {
            var arg = base.CreateParameter();
            arg.AddOption("f", String.Format("avi {0}", this.OutputFile.QuotedString()));
            return arg;
        }
    }
}