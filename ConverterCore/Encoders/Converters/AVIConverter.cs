using System;
using System.Globalization;
using ConverterCore.FFMpeg;
using ConverterCore.Processors;

namespace ConverterCore.Encoders
{
    public class AVIConverter : EncoderBase
    {
        public AVIConverter(MediaInformation mediaInfo,
                            string outputFile)
            : base(mediaInfo, outputFile)
        {

        }
        protected override FFMpegArgument CreateParameter()
        {
            var arg = base.CreateParameter();
            arg.AddOption("f", String.Format("avi {0}", this.OutputFile.QuotedString()));
            return arg;
        }

       
    }
}