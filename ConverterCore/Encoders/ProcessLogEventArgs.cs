using System;

namespace ConverterCore.Encoders
{
    public class ProcessLogEventArgs : EventArgs
    {
        public string Log { get; private set; }

        public ProcessLogEventArgs(string log)
        {
            this.Log = log;
        }
    }
}