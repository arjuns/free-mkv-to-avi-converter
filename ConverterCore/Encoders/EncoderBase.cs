using System;
using System.Globalization;
using System.IO;
using ConverterCore.FFMpeg;
using ConverterCore.Processors;

namespace ConverterCore.Encoders
{
    public abstract class EncoderBase : MediaProcessor
    {
        public MediaInformation MediaInfo { get; private set; }
        public string OutputFile { get; private set; }
        public event EventHandler<EncodeProgressEventArgs> Progress;
        public event EventHandler<EncodeFinishedEventArgs> VideoFinished;

        protected EncoderBase(MediaInformation mediaInfo, string outputFile)
        {
            this.MediaInfo = mediaInfo;
            this.OutputFile = outputFile;
        }

        protected override FFMpegArgument CreateParameter()
        {
            var arg = new FFMpegArgument();
            arg.AddOption("i", this.MediaInfo.OriginalFilePath.QuotedString());
            AdjustBitRate(arg);
            return arg;
        }
        
        
        protected virtual void DoEncodeProgress(EncodeProgressEventArgs e)
        {

            if (this.Progress != null)
            {
                Progress(this, e);
            }
        }

        protected virtual void DoEncodeFinished(EncodeFinishedEventArgs e)
        {
            if (this.VideoFinished != null)
            {
                VideoFinished(this, e);
            }

        }



        protected override void OnDataReceived(ProcessLogEventArgs e)
        {
            if (e.Log != null)
            {

                if (e.Log.StartsWith("frame"))
                {


                    EncodeProgressEventArgs epe = new EncodeProgressEventArgs();
                    epe.RawOutputLine = e.Log;
                    epe.TotalFrames = MediaInfo.TotalFrames;
                    epe.OriginalFileName = MediaInfo.OriginalFilePath;

                    string[] parts = e.Log.Split(new string[] { " ", "=" }, StringSplitOptions.RemoveEmptyEntries);
                    long lCurrentFrame;
                    Int64.TryParse(parts[1], out lCurrentFrame);
                    epe.CurrentFrame = lCurrentFrame;


                    short sFPS;
                    Int16.TryParse(parts[3], out sFPS);
                    epe.FPS = sFPS;

                    //Calculate percentage
                    double dCurrentFrame = (double)epe.CurrentFrame;
                    double dTotalFrames = (double)epe.TotalFrames;
                    double value = dCurrentFrame * 100 / dTotalFrames;
                    var sPercentage = Math.Round(value * 100, 0);
                    epe.Percentage = sPercentage;


                    DoEncodeProgress(epe);
                }

            }
        }

        protected override void OnProcessExited(ProcessExitEventArgs e)
        {
            int iExitCode = e.ExitCode;
            bool blFileExists = File.Exists(OutputFile);
            EncodeFinishedEventArgs efe = new EncodeFinishedEventArgs();
            efe.Success = (iExitCode.Equals(0) && blFileExists);
            efe.IsCancelled = this.IsCancelled;
            DoEncodeFinished(efe);
        }

        public void BeginEncode()
        {
            RunProcessAsync();
        }
        protected virtual void AdjustBitRate(FFMpegArgument arg)
        {
            if (MediaInfo.AudioBitRate > 0)
                arg.AddOption("c:a", "copy");
            if (MediaInfo.VideoBitRate > 0)
                arg.AddOption("c:v", "copy");


            if (MediaInfo.VideoBitRate < 1)
            {
                //little hack
                int h = Math.Max(MediaInfo.Height, MediaInfo.Width);
                if (h < 180) MediaInfo.VideoBitRate = 400;
                else if (h < 260) MediaInfo.VideoBitRate = 1000;
                else if (h < 400) MediaInfo.VideoBitRate = 2000;
                else if (h < 800) MediaInfo.VideoBitRate = 5000;
                else MediaInfo.VideoBitRate = 8000;
                arg.AddOption("b", MediaInfo.VideoBitRate.ToString(CultureInfo.InvariantCulture) + "k");
            }
            if (MediaInfo.AudioBitRate < 1)
            {
                arg.AddOption("ab", "128k");
            }
        }

    }
}