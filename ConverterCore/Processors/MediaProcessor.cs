using System;
using System.Threading;
using ConverterCore.Encoders;
using ConverterCore.FFMpeg;

namespace ConverterCore.Processors
{
    public abstract class MediaProcessor
    {
        private readonly FFMpegProcess ffmpeg;

        public bool IsCancelled { get; private set; }

        protected MediaProcessor()
        {
            ffmpeg = new FFMpegProcess("");
            ffmpeg.ErrorData += (a, b) => OnDataReceived(new ProcessLogEventArgs(b.Log));
            ffmpeg.ProcessFinished += (a, b) => OnProcessExited(b);
        }
        public bool IsRunning
        {
            get { return ffmpeg.IsRunning; }
        }
        public void Cancel()
        {
            ffmpeg.Kill();
            this.IsCancelled = true;
            Thread.Sleep(200);

        }
        protected string RunProcess()
        {
            try
            {
                string parameters = CreateParameter().Build();
                ffmpeg.SetArgument(parameters);
                return ffmpeg.Start();
            }
            catch (Exception)
            {
                return "";
            }


        }
        protected void RunProcessAsync()
        {
            string parameters = CreateParameter().Build();
            ffmpeg.SetArgument(parameters);
            ffmpeg.StartAsync();
        }

        protected abstract FFMpegArgument CreateParameter();

        protected virtual void OnProcessExited(ProcessExitEventArgs e)
        {

        }
        protected virtual void OnDataReceived(ProcessLogEventArgs e)
        {

        }




    }
}