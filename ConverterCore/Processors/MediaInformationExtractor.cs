using System;
using System.Text.RegularExpressions;
using ConverterCore.Encoders;
using ConverterCore.FFMpeg;

namespace ConverterCore.Processors
{
    public class MediaInformationExtractor : MediaProcessor, IMediaInfoExtractor
    {
        private readonly string filename;

        public MediaInformationExtractor(string filename)
        {
            this.filename = filename;
        }

        protected override FFMpegArgument CreateParameter()
        {
            FFMpegArgument argument=new FFMpegArgument();
            argument.AddOption("i", string.Format("\"{0}\"", filename));
            return argument;

        }

        public MediaInformation GetInfo()
        {
            return FillInfo(RunProcess());
        }
        protected override void OnDataReceived(ProcessLogEventArgs e)
        {

        }
        private MediaInformation FillInfo(string rawdata)
        {
            MediaInformation input = new MediaInformation(this.filename);
            input.RawInfo = rawdata;
            input.Duration = ExtractDuration(input.RawInfo);
            input.BitRate = ExtractBitrate(input.RawInfo);
            input.RawAudioFormat = ExtractRawAudioFormat(input.RawInfo);
            input.AudioFormat = ExtractAudioFormat(input.RawAudioFormat);
            input.RawVideoFormat = ExtractRawVideoFormat(input.RawInfo);
            input.VideoFormat = ExtractVideoFormat(input.RawVideoFormat);
            input.Width = ExtractVideoWidth(input.RawInfo);
            input.Height = ExtractVideoHeight(input.RawInfo);
            input.FrameRate = ExtractFrameRate(input.RawVideoFormat);
            input.TotalFrames = ExtractTotalFrames(input.Duration, input.FrameRate);
            input.AudioBitRate = ExtractAudioBitRate(input.RawAudioFormat);
            input.VideoBitRate = ExtractVideoBitRate(input.RawVideoFormat);
            return input;
        }
        #region Extraction methods
        protected TimeSpan ExtractDuration(string rawInfo)
        {
            TimeSpan t = new TimeSpan(0);
            Regex re = new Regex("[D|d]uration:.((\\d|:|\\.)*)", RegexOptions.Compiled);
            Match m = re.Match(rawInfo);

            if (m.Success)
            {
                string duration = m.Groups[1].Value;
                string[] timepieces = duration.Split(new char[] { ':', '.' });
                if (timepieces.Length == 4)
                {
                    t = new TimeSpan(0, Convert.ToInt16(timepieces[0]), Convert.ToInt16(timepieces[1]), Convert.ToInt16(timepieces[2]), Convert.ToInt16(timepieces[3]));
                }
            }

            return t;
        }
        protected double ExtractBitrate(string rawInfo)
        {
            Regex re = new Regex("[B|b]itrate:.((\\d|:)*)", RegexOptions.Compiled);
            Match m = re.Match(rawInfo);
            double kb = 0.0;
            if (m.Success)
            {
                Double.TryParse(m.Groups[1].Value, out kb);
            }
            return kb;
        }
        protected string ExtractRawAudioFormat(string rawInfo)
        {
            string a = string.Empty;
            Regex re = new Regex("[A|a]udio:.*", RegexOptions.Compiled);
            Match m = re.Match(rawInfo);
            if (m.Success)
            {
                a = m.Value;
            }
            return a.Replace("Audio: ", "");
        }
        protected string ExtractAudioFormat(string rawAudioFormat)
        {
            string[] parts = rawAudioFormat.Split(new string[] { ", " }, StringSplitOptions.None);
            return parts[0].Replace("Audio: ", "");
        }
        protected string ExtractRawVideoFormat(string rawInfo)
        {
            string v = string.Empty;
            Regex re = new Regex("[V|v]ideo:.*", RegexOptions.Compiled);
            Match m = re.Match(rawInfo);
            if (m.Success)
            {
                v = m.Value;
            }
            return v.Replace("Video: ", ""); ;
        }
        protected string ExtractVideoFormat(string rawVideoFormat)
        {
            string[] parts = rawVideoFormat.Split(new string[] { ", " }, StringSplitOptions.None);
            return parts[0].Replace("Video: ", "");
        }
        protected int ExtractVideoWidth(string rawInfo)
        {
            int width = 0;
            Regex re = new Regex("(\\d{2,4})x(\\d{2,4})", RegexOptions.Compiled);
            Match m = re.Match(rawInfo);
            if (m.Success)
            {
                int.TryParse(m.Groups[1].Value, out width);
            }
            return width;
        }
        protected int ExtractVideoHeight(string rawInfo)
        {
            int height = 0;
            Regex re = new Regex("(\\d{2,4})x(\\d{2,4})", RegexOptions.Compiled);
            Match m = re.Match(rawInfo);
            if (m.Success)
            {
                int.TryParse(m.Groups[2].Value, out height);
            }
            return height;
        }
        protected double ExtractFrameRate(string rawVideoFormat)
        {
            string[] parts = rawVideoFormat.Split(new string[] { ", " }, StringSplitOptions.None);

            double dFPS = 0;

            foreach (string p in parts)
            {
                if (p.ToLower().Contains("fps"))
                {
                    Double.TryParse(p.ToLower().Replace("fps", "").Replace(".", ",").Trim(), out dFPS);

                    break;
                }
                else if (p.ToLower().Contains("tbr"))
                {
                    Double.TryParse(p.ToLower().Replace("tbr", "").Replace(".", ",").Trim(), out dFPS);

                    break;
                }
            }

            //Audio: mp3, 44100 Hz, 2 channels, s16, 140 kb/s

            return dFPS;
        }
        protected double ExtractAudioBitRate(string rawAudioFormat)
        {
            string[] parts = rawAudioFormat.Split(new string[] { ", " }, StringSplitOptions.None);

            double dAbr = 0;

            foreach (string p in parts)
            {
                if (p.ToLower().Contains("kb/s"))
                {
                    Double.TryParse(p.ToLower().Replace("kb/s", "").Replace(".", ",").Trim(), out dAbr);

                    break;
                }
            }

            return dAbr;
        }
        protected double ExtractVideoBitRate(string rawVideoFormat)
        {
            string[] parts = rawVideoFormat.Split(new string[] { ", " }, StringSplitOptions.None);

            double dVBR = 0;

            foreach (string p in parts)
            {
                if (p.ToLower().Contains("kb/s"))
                {
                    Double.TryParse(p.ToLower().Replace("kb/s", "").Replace(".", ",").Trim(), out dVBR);

                    break;
                }
            }

            return dVBR;
        }
        protected long ExtractTotalFrames(TimeSpan duration, double frameRate)
        {
            return (long)Math.Round(duration.TotalSeconds * frameRate, 0);
        }
        #endregion
    }
}