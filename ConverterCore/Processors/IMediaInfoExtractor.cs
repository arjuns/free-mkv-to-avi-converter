using ConverterCore.FFMpeg;

namespace ConverterCore.Processors
{
    public interface IMediaInfoExtractor
    {
        MediaInformation GetInfo();
    }
}