using System;
using System.Globalization;
using System.IO;
using ConverterCore.Encoders;
using ConverterCore.FFMpeg;

namespace ConverterCore.Processors
{
    public class ThumbnailExtractor : MediaProcessor
    {
        public string Filename { get; private set; }
        public string OutputFileName { get; private set; }
        public MediaInformation MediaInformation { get; private set; }

        public ThumbnailExtractor(string filename, string outputFileName, MediaInformation mediaInformation)
        {
            this.Filename = filename;
            this.OutputFileName = outputFileName;
            MediaInformation = mediaInformation;
        }

        public bool ExtractThumbnal()
        {
            string log = RunProcess();
            if (String.IsNullOrEmpty(log))
                return false;
            return File.Exists(OutputFileName);
        }

        protected override FFMpegArgument CreateParameter()
        {
            int secs = (int)Math.Round(TimeSpan.FromTicks(MediaInformation.Duration.Ticks / 3).TotalSeconds, 0);
            if (secs == 0)
                secs = 1;

            FFMpegArgument arg = new FFMpegArgument();
            arg.AddOption("y");
            arg.AddOption("ss", secs.ToString(CultureInfo.InvariantCulture));
            arg.AddOption("i", Filename.QuotedString());
            arg.AddOption("s", String.Format("63x48 {0}", OutputFileName.QuotedString()));
            arg.AddOption("r", 1.ToString(CultureInfo.InvariantCulture));
            arg.AddOption("vframes", 1.ToString(CultureInfo.InvariantCulture));
            arg.AddOption("an");
            arg.AddOption("vcodec", "mjpeg");

            return arg;
        }
    }

    public static class StringHelper
    {
        public static string QuotedString(this string source)
        {
            return "\"" + source + "\"";
        }
    }




}